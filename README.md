## Instructions
* Make sure you have PIP installed, if not then install installed
    
    ```
    sudo su
    apt-get update
    apt-get install python3-pip
    ```
* Installing pymongo
    
    ```pip3 install pymongo```
* Clone the code

    ```
    git clone https://gitlab.com/konnectchetan/azure-cosmosdb-mongoapi-python
    cd azure-cosmosdb-mongoapi-python
    ```
* To get the connection string
    - Go to your Cosomos DB page
    - In the left pane, click on `Quick Start`
    - Click on Python
    - There you will see your `Connection String`
* Update the `conenction string`, `database name`, and `collection name` in `read_data.py`
    
    ```vi read_data.py```
* Now, execute the program using
    
    ```python3 read_data.py```
