from pymongo import MongoClient

uri = "YOUR_DB_CONNECTION_STRING"

#Creating a db client
client = MongoClient(uri)

#selecting db
db = client.DATABASE_NAME

#select collection
col = db.COLLECTION_NAME

#get documents
for item in col.find():
  print(item)
